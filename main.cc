#include<iostream>
#include<unistd.h>
#include<cstdlib>
#include<time.h>
#include<assert.h>

#include <fcntl.h>
#include<mpi.h>

#define MAX_TIME 1000

#define MASTER 0

// int increment(int &iter,int &niter,
// 	      MPI_Win &WCounter, MPI_Win &WLock)
// {

//   int pid;
//   MPI_Comm_rank(MPI_COMM_WORLD, &pid);

//   int unlock;
//   do {
    
//     int zero = 0;
//     MPI_Win_lock(MPI_LOCK_EXCLUSIVE,0,0,WLock);
//     //MPI_Fetch_and_op(&zero, &unlock, MPI_INT, 0, 0, MPI_REPLACE, WLock);
//     MPI_Get(&unlock, 1, MPI_INT, 0, 0, 1, MPI_INT, WLock);
//     MPI_Win_flush_local(0,WLock);
//     MPI_Accumulate(&zero, 1, MPI_INT, 0, 0, 1,
//   		   MPI_INT, MPI_REPLACE, WLock);    
//     MPI_Win_unlock(0,WLock);

//     usleep(1);
    
//   } while (unlock == 0);
  
//   MPI_Win_lock(MPI_LOCK_EXCLUSIVE,0,0,WCounter);
//   MPI_Get(&iter, 1, MPI_INT, 0, 0, 1, MPI_INT, WCounter);
//   MPI_Win_unlock(0,WCounter);

//   if (iter < niter) {
//     int one = 1;
//     MPI_Win_lock(MPI_LOCK_EXCLUSIVE,0,0,WCounter);
//     MPI_Accumulate(&one, 1, MPI_INT, 0, 0, 1,
// 		   MPI_INT, MPI_SUM, WCounter);
//     MPI_Win_unlock(0,WCounter);
//   }

//   unlock = 1;
//   MPI_Win_lock(MPI_LOCK_EXCLUSIVE,0,0,WLock);
//   MPI_Put(&unlock, 1, MPI_INT, 0, 0, 1, MPI_INT, WLock);
//   MPI_Win_unlock(0,WLock);  
// }


int increment(int &iter,int &niter,MPI_Win &WCounter,
	      int &fd, struct flock &fl)
{
  int pid;
  MPI_Comm_rank(MPI_COMM_WORLD, &pid);
  
  while (fcntl(fd, F_SETLK, &fl) == -1) {
    usleep(1);
  }

  fl.l_type = F_UNLCK;

  MPI_Win_lock(MPI_LOCK_EXCLUSIVE,0,0,WCounter);  
  MPI_Get(&iter, 1, MPI_INT, 0, 0, 1, MPI_INT, WCounter);
  MPI_Win_unlock(0,WCounter);

  if (iter < niter) {
    int one = 1;
    MPI_Win_lock(MPI_LOCK_EXCLUSIVE,0,0,WCounter);  
    MPI_Accumulate(&one, 1, MPI_INT, 0, 0, 1,
		   MPI_INT, MPI_SUM, WCounter);
    MPI_Win_unlock(0,WCounter);
  }

  //unlock
  if (fcntl(fd, F_SETLK, &fl) == -1)
    assert(0);  
  fl.l_type = F_WRLCK;

}

int main(int argc, char **argv)
{
  int niter = 25;

  int pid, nproc;
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &pid);
  MPI_Comm_size(MPI_COMM_WORLD, &nproc);

  int giter=nproc-1;
  MPI_Win WCounter;
  MPI_Win_create(&giter,sizeof(int),sizeof(int),
		 MPI_INFO_NULL,MPI_COMM_WORLD,&WCounter);
    
  int iter;
  iter = pid-1;

  int fd;
  fd = open("lock.aux", O_RDWR | O_CREAT, S_IRWXU);
  struct flock fl;
  fl.l_type=F_WRLCK;
  fl.l_whence=SEEK_SET;
  fl.l_start=0;
  fl.l_len=1;
 
  srand(time(NULL)+pid);

  if (pid != MASTER) {
    
    do {
      int wait = rand() % MAX_TIME + 1;
      std::cout << "pid " << pid << " "
		<< "got iter " << iter << " and "
		<< "computer for " << wait << " ms."
		<< std::endl;
      usleep(wait);
      increment(iter,niter,WCounter,fd,fl);
    } while (iter < niter);
    
  }
  
  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Finalize();
  if (pid == 0)
    std::cout << "Congratulations! Program ended succesfully."
	      << std::endl;
  return 0;
}
